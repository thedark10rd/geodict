import argparse, json, sys
from elasticsearch import Elasticsearch,helpers
from elasticsearch import helpers
import copy
from tqdm import tqdm
from mytoolbox.text.size import wc_l


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("input", help="give input json file")
    parser.add_argument("-e", "--es_host", help="Elasticsearch Host address", default="127.0.0.1")
    parser.add_argument("-p", "--es_port", help="Elasticsearch Host port", default="9200")
    args = parser.parse_args()

    if not os.path.exists(args.input):
        raise FileNotFoundError("Input File '{0}' not found !".format(args.input))

    file_name = args.input
    es_client = Elasticsearch(args.es_host)

    if not es_client.ping():
        raise ConnectionError("Could not connect to Elasticserver at {0}".format(args.es_host))

    # If exists in the dataase, delete !
    if es_client.indices.exists(index="gazetteer"):
        es_client.indices.delete(index="gazetteer")

    # Open input file
    gazetteer = open(file_name, encoding='utf-8')

 
    mappings = json.load(open("config/mappings.json")) 
    # complete Mapping depending on custom properties extracted
    property_to_be_mapped = json.load(open('config/configuration.json'))
    for prop in property_to_be_mapped["properties_to_extract"]:
        mappings['mappings']['_default_']['properties'][prop['id']] = {'type':prop["mappings"]}
        if prop["mappings_details"]:
            for k,v in prop["mappings_details"].items():
                mappings['mappings']['_default_']['properties'][prop['id']][k]=v
    print("Mapping of Geodict index: ", mappings)

    # Creation of the index in Elasticsearch databased
    es_client.indices.create(index="gazetteer", body=mappings)
    action_list=[]

    number_of_entries = wc_l(file_name)

    for line in tqdm(gazetteer,desc="Importing ...",total=number_of_entries):
        data = json.loads(line.strip())
        if '_score' in data.keys():
            data['score'] = data['_score']
            del data['_score']
        if "geometry" in data:
            del data["geometry"] # Difficult with ES ... so we delete it
        if "coord" in data:
            data["coord"]["lat"]=float(data["coord"]["lat"])
            data["coord"]["lon"]= float(data["coord"]["lon"])

            if data["coord"]["lat"] >90 or data["coord"]["lon"] >180:continue

        if not data["fr"]:continue

        actions = {
        "_index": "gazetteer",
        "_type": "place",
        "_source": data
        }
        action_list.append(actions)
        if len(action_list) % 1000 == 0:
            helpers.bulk(es_client,action_list,request_timeout=30)
            sys.stdout.write("\rEntity transferred: " + '{:,}'.format(i))
            action_list = []


if __name__ == '__main__':
    main()
